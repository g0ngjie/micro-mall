#!/bin/bash

git pull origin master \
&& npm install \
&& docker-compose down \
&& docker rmi yanzi_image \
&& docker-compose build \
&& docker-compose up -d