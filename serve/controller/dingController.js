"use strict";

const { DingUtil, KLayout } = require("../utils");

exports.getJsConfig = async (ctx, next) => {
  const data = await DingUtil.getDingConfig();
  await KLayout.layout(ctx, data);
  await next();
};

exports.login = async (ctx, next) => {
  const { code } = ctx.query;
  const authData = await DingUtil.OAuthLogin(code);
  await KLayout.layout(ctx, authData);
  await next();
};
